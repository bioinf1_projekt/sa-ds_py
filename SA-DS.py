from utils import *


def SA_DS(s: np.ndarray, sa: np.ndarray, n: int, k: int, cs: int):
    # LS-type array in bits
    t: np.ndarray = np.empty(n // 8 + 1, dtype=np.int8)

    t_set(t, n - 2, 0)
    t_set(t, n - 1, 1)
    # reduce the problem by at least half
    # determine the type of each character
    for i in range(n - 3, -1, -1):
        chr1: int = get_chr(s, i, cs)
        chr2: int = get_chr(s, i + 1, cs)
        tget: int = t_get(t, i + 1)
        t_set(t, i, 1 if chr1 < chr2 or (chr1 == chr2 and tget == 1) else 0)

    # 2n1 must be smaller or equal to n
    sa1: np.ndarray = sa
    n1: int = num_critical_chars(t, n, sa1, 3)
    s1: np.ndarray = sa[n - n1: n]

    # bucket array that is used for bucket sorting
    bkt: np.ndarray = np.zeros(k + 1, dtype=np.int32)

    # bucket sort that sorts gamma-weighted fixed size
    bucket_sort_ls(sa1, s1, t, n, n1, 4)
    bucket_sort(s1, sa1, s, n, cs, n1, k, bkt, 4)
    bucket_sort(sa1, s1, s, n, cs, n1, k, bkt, 3)
    bucket_sort(s1, sa1, s, n, cs, n1, k, bkt, 2)
    bucket_sort(sa1, s1, s, n, cs, n1, k, bkt, 1)
    bucket_sort(s1, sa1, s, n, cs, n1, k, bkt, 0)

    # Assign the elements of s1 to the initial n1 positions
    # even elements in SA
    for i in range(n1 - 1, -1, -1):
        j: int = 2 * i
        sa[j] = sa1[i]
        sa[j + 1] = -1

    for i in range(2 * (n1 - 1) + 3, n, 2):
        sa[i] = -1

    # Assign names to the sorted substrings
    name: int = 0
    c: np.ndarray = np.full(5, -1)

    for i in range(n1):
        pos: int = sa[2 * i]
        diff: bool = False

        for h in range(4):
            if get_chr(s, pos + h, cs) != c[h]:
                diff = True
                break

        if omega_weight(s, t, pos + 4, cs) != c[4]:
            diff = True

        if diff:
            name += 1

            for h in range(4):
                c[h] = get_chr(s, pos + h, cs) if pos + h < n else -1

            c[4] = omega_weight(s, t, pos + 4, cs) if pos + 4 < n else -1

        if pos % 2 == 0:
            pos -= 1

        sa[pos] = name - 1
    # pack s1
    j: int = n - 1
    for i in range(n // 2 * 2 - 1, -1, -2):
        if j < 0:
            break

        if sa[i] != -1:
            sa[j] = sa[i]
            j -= 1
    # Resolve the reduced problem
    if name < n1:
        # create an array with a size four times that of s1 to accommodate each integer value as a four-byte value.
        byte_array: np.ndarray = np.empty(len(s1) * 4, dtype=np.int8)

        for idx in range(len(s1)):
            # Convert int to 4-byte array
            buffer: bytes = s1[idx].to_bytes(4, 'big')
            for insideIdx in range(len(buffer)):
                byte_array[(4 * idx) + insideIdx] = buffer[insideIdx]

        SA_DS(byte_array, sa1, n1, name - 1, 4)
    else:
        for i in range(n1):
            sa1[s1[i]] = i

    # Obtain the final result
    # get p1 into s1
    num_critical_chars(t, n, s1, 3)
    bkt: np.ndarray = np.zeros(k + 1, dtype=np.int32)

    # arrange LMS characters into their buckets
    # Locate the endpoints of the buckets
    get_buckets(s, bkt, n, k, cs, True)

    # get index from s1 that stores p1
    for i in range(n1):
        sa1[i] = s1[sa1[i]]

    # initiate SA starting with n1, finnishing with n-1
    for i in range(n1, n):
        sa[i] = -1

    for i in range(n1 - 1, -1, -1):
        j = sa[i]
        sa[i] = -1

        if j > 0 and t_get(t, j) != 0 and t_get(t, j - 1) == 0:
            char: int = get_chr(s, j, cs)
            bkt[char] -= 1
            sa[bkt[char]] = j

    # Compute SA1
    # Locate the startpoints of buckets
    get_buckets(s, bkt, n, k, cs, False)

    for i in range(n):
        j = sa[i] - 1

        if j >= 0 and t_get(t, j) == 0:
            sa[bkt[get_chr(s, j, cs)]] = j
            bkt[get_chr(s, j, cs)] += 1

    # Compute SAs
    # Locate the endpoints of buckets
    get_buckets(s, bkt, n, k, cs, True)

    for i in range(n - 1, -1, -1):
        j = sa[i] - 1

        if j >= 0 and t_get(t, j) != 0:
            bkt[get_chr(s, j, cs)] -= 1
            sa[bkt[get_chr(s, j, cs)]] = j
