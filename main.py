from SA_DS import SA_DS
import numpy as np
import sys
from timeit import default_timer as timer

if __name__ == '__main__':
    args = sys.argv
    fileName = args[1]

    with open(fileName, 'r') as e_coli_file:
        buffer = bytearray((e_coli_file.read() + '$').encode('utf-8'))
    s = np.frombuffer(buffer, dtype=np.int8)
    sa = np.full(len(s), None, dtype=object)
    n = len(s)

    start = timer()

    SA_DS(s, sa, n, 128, 1)

    end = timer()
    print(end - start)

    with open('result.txt', 'w+') as file:
        file.write(', '.join(list(map(str, sa))))
