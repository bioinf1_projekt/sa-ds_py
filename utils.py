import numpy as np

min_int = -2147483648
mask = np.array([0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01], dtype=np.uint8)


def t_get(t: np.ndarray, i: int) -> int:
    return 1 if (t[i // 8] & mask[i % 8]) else 0


def t_set(t: np.ndarray, i: int, b: int):
    if b:
        t[i // 8] |= mask[i % 8]
    else:
        t[i // 8] &= ~mask[i % 8]


def get_chr(s: np.ndarray, i: int, cs: int) -> int:
    if cs == 4:
        return int.from_bytes(s[i * 4: i * 4 + 4], byteorder='big', signed=False)
    else:
        return s[i] & 0xFF


def omega_weight(s: np.ndarray, t: np.ndarray, x: int, cs: int) -> int:
    if x * 4 >= len(s):
        return min_int
    return get_chr(s, x, cs) * 2 + t_get(t, x)


def num_critical_chars(t: np.ndarray, n: int, p1: np.ndarray, d: int) -> int:
    i: int = -1
    j: int = 0

    while i < n - 1:
        h: int = 0
        is_lms: bool = False

        for h in range(2, d + 2):
            if t_get(t, i + h - 1) == 0 and t_get(t, i + h) == 1:
                is_lms = True
                break

        if j == 0 and not is_lms:
            i += d
            continue

        i += h if is_lms else d

        if p1 is not None:
            p1[j] = i

        j += 1

    return j


def bucket_sort_ls(src: np.ndarray, dst: np.ndarray, t: np.ndarray, n: int, n1: int, h: int):
    c: np.ndarray = np.array([0, n1 - 1])

    for i in range(n1):
        j: int = src[i] + h

        if j > n - 1:
            j = n - 1

        if t_get(t, j) != 0:
            dst[c[1]] = src[i]
            c[1] -= 1
        else:
            dst[c[0]] = src[i]
            c[0] += 1


def bucket_sort(src: np.ndarray, dst: np.ndarray, s: np.ndarray, n: int, cs: int, n1: int, k: int, c: np.ndarray,
                d: int):
    summ: int = 0

    c.fill(0)

    for i in range(n1):
        j: int = src[i] + d

        if j > n - 1:
            j = n - 1

        temp = get_chr(s, j, cs)
        c[get_chr(s, j, cs)] += 1

    for i in range(k + 1):
        length: int = c[i]
        c[i] = summ
        summ += length

    for i in range(n1):
        j: int = src[i] + d

        if j > n - 1:
            j = n - 1

        dst[c[get_chr(s, j, cs)]] = src[i]
        c[get_chr(s, j, cs)] += 1


def get_buckets(s: np.ndarray, bkt: np.ndarray, n: int, k: int, cs: int, end: bool):
    summ: int = 0

    bkt.fill(0)

    for i in range(n):
        bkt[get_chr(s, i, cs)] += 1

    for i in range(k + 1):
        summ += bkt[i]
        bkt[i] = summ if end else summ - bkt[i]
